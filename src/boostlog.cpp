#include <common_tools/boostlog.hpp>

#include "boostlogtools.h"

namespace commtools
{

template <class T>
boost::shared_ptr<T> to_boost(std::shared_ptr<T>&& p)
{
    return boost::shared_ptr<T>(p.get(), [p](...) mutable { p.reset(); });
}

const std::vector<std::string> BoostLog::m_sev_strings = {"trace",   "debug", "info",
                                                          "warning", "error", "fatal"};

const int BoostLog::rot_size = 20 * 1024 * 1024;

INIT_SINGLETONE(BoostLog)

sev_lvl BoostLog::get_sev_lvl(const std::string& sevLvl)
{
    auto it = std::find(m_sev_strings.begin(), m_sev_strings.end(), sevLvl);

    if (it == m_sev_strings.end())
    {
        return sev_lvl::warning;
    }
    return static_cast<sev_lvl>(std::distance(m_sev_strings.begin(), it));
}

void BoostLog::rotate_general_file()
{
    m_g_file_sink->locked_backend()->rotate_file();
}

void BoostLog::init(const sev_lvl sevLvl_in, const std::string& general_filename,
                    const std::string&                                      log_path_in,
                    const std::vector<std::pair<std::string, std::string>>& channels_files,
                    const bool exclude_from_general, const std::string& custom_sink_type)
{
    m_g_file_sink = nullptr;

    m_sevLvl   = sevLvl_in;
    m_log_path = log_path_in;

    // формат общего логгера

    auto fmtGeneral = MAKE_FORMAT();

    // настройка тривиального лога

    m_lvlTriv = logging::trivial::trace;
    switch (m_sevLvl)
    {
    case sev_lvl::trace:
        m_lvlTriv = logging::trivial::trace;
        break;
    case sev_lvl::debug:
        m_lvlTriv = logging::trivial::debug;
        break;
    case sev_lvl::info:
        m_lvlTriv = logging::trivial::info;
        break;
    case sev_lvl::warning:
        m_lvlTriv = logging::trivial::warning;
        break;
    case sev_lvl::error:
        m_lvlTriv = logging::trivial::error;
        break;
    case sev_lvl::critical:
        m_lvlTriv = logging::trivial::fatal;
        break;
    }

    std::vector<std::string> exclude_ch;

    if (exclude_from_general)
    {
        for (const auto& ch : channels_files)
        {
            exclude_ch.push_back(ch.first);
        }
    }

    auto filter =
        MAKE_SEV_FILTER(m_lvlTriv, m_sevLvl) &&
        (!expr::has_attr(channel) ||
         (expr::has_attr(channel) &&
          !boost::phoenix::bind(&check_channels, expr::attr<std::string>("Channel"), exclude_ch)));

    // консоль с общим логом
    logging::add_console_log(std::cout, keywords::format = fmtGeneral, keywords::filter = filter);

    // файл с общим логом

    if (!m_log_path.empty())
    {
        if (m_g_file_sink)
        {
            logging::core::get()->remove_sink(m_g_file_sink);
            m_g_file_sink.reset();
        }

        m_g_file_sink = logging::add_file_log(
            keywords::file_name = m_log_path + general_filename, keywords::filter = filter,
            keywords::format = fmtGeneral, keywords::auto_flush = true,
            keywords::open_mode = std::ios_base::app);

        for (const auto& ch : channels_files)
        {
            auto filter_loc = MAKE_SEV_FILTER(m_lvlTriv, m_sevLvl) &&
                              (expr::has_attr(channel) &&
                               boost::phoenix::bind(&check_channel,
                                                    expr::attr<std::string>("Channel"), ch.first));

            logging::add_file_log(keywords::file_name     = m_log_path + ch.second + ".log",
                                  keywords::rotation_size = rot_size, keywords::filter = filter_loc,
                                  keywords::format = fmtGeneral, keywords::auto_flush = true);
        }
    }

    if (custom_sink_type != "")
    {
        auto result =
            boost::make_shared<sink_custom_t>(boost::make_shared<CustomSink>(custom_sink_type, ""));
        result->set_formatter(fmtGeneral);
        result->set_filter(filter);

        boost::shared_ptr<logging::core> core = logging::core::get();

        core->add_sink(result);
    }

    logging::add_common_attributes();
}

boost::shared_ptr<sink_t> get_sink(const std::string& filename, const int rot_size)
{
    auto fmt = MAKE_FORMAT();

    return logging::add_file_log(keywords::file_name = filename, keywords::rotation_size = rot_size,
                                 keywords::format = fmt, keywords::auto_flush = true);
}

boost::shared_ptr<sink_custom_t> get_sink(const std::string& tag, const std::string& type)
{
    auto fmt = MAKE_FORMAT();

    auto result = boost::make_shared<sink_custom_t>(boost::make_shared<CustomSink>(type, tag));
    result->set_formatter(fmt);

    boost::shared_ptr<logging::core> core = logging::core::get();

    core->add_sink(result);

    return result;
}

template <class Tmap, class... Args>
void BoostLog::add_current_thread_filter_common(Tmap& map, const std::string& tag,
                                                const Args&... args)
{
    auto current_id = boost::log::aux::this_thread::get_id();

    auto it = map.find(tag);

    if (it == map.end())
    {
        map[tag] = std::make_pair(get_sink(args...), std::set<thread_id_t>{});
    }

    auto& sink = map.at(tag);

    sink.second.insert(current_id);

    set_thread_filter(m_lvlTriv, m_sevLvl, sink);
}

template <class Tmap>
void BoostLog::remove_current_thread_filter_common(Tmap& map, const std::string& tag)
{
    auto it = map.find(tag);
    if (it == map.end())
    {
        return;
    }
    auto current_id = boost::log::aux::this_thread::get_id();

    it->second.second.erase(current_id);

    set_thread_filter(m_lvlTriv, m_sevLvl, it->second);
}

void BoostLog::add_current_thread_filter(const std::string& tag, const std::string& type)
{
    std::lock_guard<std::mutex> lock(m_add_thread_file_mitex);

    if ("FILE" == type)
    {
        add_current_thread_filter_common(m_files_map, tag, m_log_path + tag, rot_size);
        return;
    }
    add_current_thread_filter_common(m_custom_map, tag, tag, type);
}

void BoostLog::remove_current_thread_filter(const std::string& tag, const std::string& type)
{
    std::lock_guard<std::mutex> lock(m_add_thread_file_mitex);

    if ("FILE" == type)
    {
        remove_current_thread_filter_common(m_files_map, tag);
        return;
    }
    remove_current_thread_filter_common(m_custom_map, tag);
}
} // namespace commtools
