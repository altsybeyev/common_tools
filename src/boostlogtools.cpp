#include "boostlogtools.h"

namespace commtools
{
std::ostream& operator<<(std::ostream& strm, sev_lvl level)
{
    if (static_cast<size_t>(level) < BoostLog::m_sev_strings.size())
    {
        strm << "[" << BoostLog::m_sev_strings[static_cast<int>(level)] << "] ";
        for (size_t i = 0; i < 7 - BoostLog::m_sev_strings[static_cast<int>(level)].size(); i++)
        {
            strm << " ";
        }
    }
    else
    {
        strm << static_cast<int>(level);
    }

    return strm;
}

std::string format_channel(logging::value_ref<std::string> const& filename)
{
    size_t max_size = 11;

    std::stringstream out;

    auto channel_name = filename.get();

    if (channel_name.size() > max_size)
    {
        channel_name = channel_name.substr(0, max_size);
    }

    out << "<" << channel_name << "> ";
    for (size_t i = 0; i < max_size - channel_name.size(); i++)
    {
        out << " ";
    }
    return out.str();
}

bool check_channels(logging::value_ref<std::string> const& channel_name,
                    const std::vector<std::string>&        channels)
{
    for (const auto& ch : channels)
    {
        if (channel_name.get() == ch)
        {
            return true;
        }
    }
    return false;
}

bool check_channel(logging::value_ref<std::string> const& channel_name, const std::string& channel)
{
    return channel_name.get() == channel;
}
bool check_thread(
    const std::set<thread_id_t>&                                                  thread,
    const logging::value_ref<logging::attributes::current_thread_id::value_type>& c_thread_id)
{
    return std::find(thread.begin(), thread.end(), c_thread_id) == thread.end() ? false : true;
}

}
