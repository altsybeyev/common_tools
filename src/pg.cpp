#include <common_tools/pg.hpp>

#include <boost/algorithm/string/replace.hpp>

namespace commtools
{
std::string safe_pg_string(const std::string& input)
{
    auto new_str = input;

    boost::replace_all(new_str, "\"", "\\\"");

    return new_str;
}
std::string safe_pg_string_single_quot(const std::string& input)
{
    auto new_str = input;

    boost::replace_all(new_str, "\'", "''");

    return new_str;
}
}