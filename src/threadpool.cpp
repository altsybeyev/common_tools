#include <common_tools/boostlog.hpp>
#include <common_tools/threadpool.hpp>

#include "threadpoolworker.h"

namespace commtools
{

std::unique_ptr<ThreadPool> ThreadPool::m_instance = nullptr;

std::shared_ptr<Worker> ThreadPullDifferentQueue::getFreeWorker()
{
    std::shared_ptr<Worker> pWorker = nullptr;

    size_t minTasks = UINT32_MAX;

    for (auto& it : m_workers)
    {
        if (it->isEmpty())
        {
            return it;
        }
        else if (minTasks > it->getTaskCount())
        {
            minTasks = it->getTaskCount();
            pWorker  = it;
        }
    }

    if (get_parent_worker() && !pWorker->isEmpty())
    {
        m_workers.push_back(std::make_shared<Worker>(m_observers));
        m_workers.back()->init();

#ifndef DONT_USE_BOOSTLOG
        BL_WARNING_CH("THPOOL") << "Force thread pool enlargement, current size = "
                                << m_workers.size();
#endif

        pWorker = m_workers.back();
    }
    return pWorker;
}

void ThreadPullSharedQueue::appendFn(const std::function<void()>& fn, const std::string& tag)
{
    auto it = std::find_if(m_workers.begin(), m_workers.end(),
                           [&](const auto& w) { return w->isEmpty(); });

    if (get_parent_worker() && it == m_workers.end())
    {
        // m_workers.push_back(std::make_shared<WorkerSharedQueue>(
        //     m_observers, &ThreadPullSharedQueue::thread_fn, this));
        m_workers.push_back(std::make_shared<WorkerSharedQueue>(m_observers));
        m_workers.back()->init(&ThreadPullSharedQueue::thread_fn, this);

#ifndef DONT_USE_BOOSTLOG
        BL_WARNING_CH("THPOOL") << "Force thread pool enlargement, current size = "
                                << m_workers.size();
#endif
    }

    boost::unique_lock<boost::mutex> lock(m_mutex);
    m_fqueue.push(std::make_pair(fn, tag));
    lock.unlock();
    m_cv.notify_one();
}

void ThreadPullDifferentQueue::appendFn(const std::function<void()>& fn, const std::string& tag)
{
    boost::lock_guard<boost::mutex> lock(m_mutex);

    auto pWorker = getFreeWorker();

    if (!pWorker)
    {
        throw std::runtime_error("Error get free worker");
    }
    pWorker->appendFn(fn, tag);
}

ThreadPool& ThreadPool::get(const int threads, const std::vector<std::string> m_observers,
                            const bool is_common) noexcept
{
    if (!m_instance)
    {
        if (is_common)
        {
            m_instance.reset(new ThreadPullSharedQueue(threads, m_observers));
        }
        else
        {
            m_instance.reset(new ThreadPullDifferentQueue(threads, m_observers));
        }
    }
    return *m_instance;
}

ThreadPool& ThreadPool::get()
{
    if (!m_instance)
    {
        throw std::runtime_error("Thread pull is not created");
    }
    return *m_instance;
}

ThreadPullSharedQueue::ThreadPullSharedQueue(const int                       threads,
                                             const std::vector<std::string>& observers) noexcept
    : ThreadPoolT<WorkerSharedQueue>(observers), therminate_pool(false)
{
    init(threads, &ThreadPullSharedQueue::thread_fn, this);
}

void ThreadPullSharedQueue::wait_concrete()
{
    while (!m_fqueue.empty())
    {
        boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
    }
}

void ThreadPullDifferentQueue::wait_concrete()
{
}

void ThreadPullSharedQueue::thread_fn(bool& in_work, std::string& current_tag)
{
    while (true)
    {
        in_work = false;
        boost::unique_lock<boost::mutex> lock(m_mutex);
        m_cv.wait(lock, [&] { return !m_fqueue.empty() || therminate_pool; });
        in_work = true;

        if (therminate_pool && m_fqueue.empty())
        {
            return;
        }

        auto fn = m_fqueue.front();
        m_fqueue.pop();
        lock.unlock();

        current_tag = fn.second;

        for (const auto& obs : m_observers)
        {
            BoostLog::get().add_current_thread_filter(current_tag, obs);
        }

        fn.first();

        for (const auto& obs : m_observers)
        {
            BoostLog::get().remove_current_thread_filter(current_tag, obs);
        }

        current_tag = "";
    }
};

void ThreadPullSharedQueue::terminate()
{
    therminate_pool = true;
    m_cv.notify_all();
}
void ThreadPullDifferentQueue::terminate()
{
    for (auto& it : m_workers)
    {
        it->terminate();
    }
}

ThreadPullSharedQueue::~ThreadPullSharedQueue()
{
    terminate();
    boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
}

ThreadPullDifferentQueue::ThreadPullDifferentQueue(
    const int threads, const std::vector<std::string>& observers) noexcept
    : ThreadPoolT<Worker>(observers)
{
    init(threads);
}

} // namespace commtools
