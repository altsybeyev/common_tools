#include <common_tools/algorithm.hpp>
#include <webtools/cookies.hpp>

namespace webtools
{
std::string get_cookie(const std::string& key, const std::string& data)
{
    const auto res = commtools::find_between_patterns_content(data, key + "=", ";");
    return res.second.empty() ? res.first : "";
}
} // namespace webtools
