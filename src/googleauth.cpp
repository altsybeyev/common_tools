#include <webtools/googleauth.hpp>

#include <webtools/curl_request.hpp>

#include <common_tools/boostlog.hpp>
#include <serreflection/read_json.hpp>

struct AuthInfo
{
    std::string id_token;
    std::string mode;
};

SERIALIZIBLE_STRUCT(AuthInfo, srfl::CheckModes::FATAL,
                    (std::string, id_token, DEF_D())(std::string, mode, DEF_D()))

namespace webtools
{

#define RETURN_ON_FAIL_MSG(cond, msg)                                                              \
    if (cond)                                                                                      \
    {                                                                                              \
        BL_ERROR() << msg;                                                                         \
        result.second = msg;                                                                       \
        return result;                                                                             \
    }

#define RETURN_ON_FAIL(cond) RETURN_ON_FAIL_MSG(cond, err_msg)

std::pair<std::unique_ptr<AuthInfoResult>, std::string>
check_get_google_auth(const std::string& tocken, const std::map<std::string, std::string>& ids,
                      const std::string& host)
{
    BL_FTRACE() << "log_in::host: " << host;

    const auto err_msg = "Invalid request: ";

    std::pair<std::unique_ptr<AuthInfoResult>, std::string> result;
    result.first = nullptr;

    auto host_id_it = ids.find(host);
    RETURN_ON_FAIL_MSG(host_id_it == ids.end(), "unsupported host: " + host)

    const auto curl_result = perform_curl_request(
        std::string("https://oauth2.googleapis.com/tokeninfo?id_token=" + tocken));

    RETURN_ON_FAIL_MSG(!curl_result.second.empty(), "error in perform curl request")

    auto auth_info_result = srfl::read_json_string<AuthInfoResult>(curl_result.first);
    RETURN_ON_FAIL_MSG(!auth_info_result, "error in google token data get")

#ifdef PROD
    RETURN_ON_FAIL_MSG(auth_info_result->aud != host_id_it->second, "invalid app id")
#endif

    RETURN_ON_FAIL_MSG(auth_info_result->iss != "accounts.google.com" &&
                           auth_info_result->iss != "https://accounts.google.com",
                       "invalid token issurer")

    auto time = std::time(nullptr);

    BL_FTRACE() << "log_in::current time: " << time;
    BL_FTRACE() << "log_in::token expired time: " << auth_info_result->exp;

    RETURN_ON_FAIL_MSG(auth_info_result->exp < time, "token expired")

    auth_info_result->login_as = "google";

    result.first = std::move(auth_info_result);

    return result;
}
} // namespace webtools