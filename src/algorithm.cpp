#include <common_tools/algorithm.hpp>

namespace commtools
{

PointLL::PointLL(const double la, const double lo) : lat(la), lon(lo)
{
}

std::vector<PointLL> decode(const std::string& encoded, int precision)
{
    double kPolylinePrecision = std::pow(10, precision);

    double kInvPolylinePrecision = 1.0 / kPolylinePrecision;

    size_t i = 0; // what byte are we looking at

    // Handy lambda to turn a few bytes of an encoded string into an integer
    auto deserialize = [&encoded, &i](const int previous) {
        // Grab each 5 bits and mask it in where it belongs using the shift
        int byte, shift = 0, result = 0;
        do
        {
            byte = static_cast<int>(encoded[i++]) - 63;
            result |= (byte & 0x1f) << shift;
            shift += 5;
        } while (byte >= 0x20);
        // Undo the left shift from above or the bit flipping and add to previous
        // since its an offset
        return previous + (result & 1 ? ~(result >> 1) : (result >> 1));
    };

    // Iterate over all characters in the encoded string
    std::vector<PointLL> shape;
    int                  last_lon = 0, last_lat = 0;
    while (i < encoded.length())
    {
        // Decode the coordinates, lat first for some reason
        int lat = deserialize(last_lat);
        int lon = deserialize(last_lon);

        // Shift the decimal point 5 places to the left
        shape.emplace_back(static_cast<double>(static_cast<double>(lat) * kInvPolylinePrecision),
                           static_cast<double>(static_cast<double>(lon) * kInvPolylinePrecision));

        // Remember the last one we encountered
        last_lon = lon;
        last_lat = lat;
    }
    return shape;
}

int py2_round(double value)
{
    // Google's polyline algorithm uses the same rounding strategy as Python 2, which is different
    // from JS for negative values
    return std::floor(std::abs(value) + 0.5) * (value >= 0 ? 1 : -1);
}

std::string encode(double current, double previous, double factor)
{
    current        = py2_round(current * factor);
    previous       = py2_round(previous * factor);
    int coordinate = current - previous;
    coordinate <<= 1;
    if (current - previous < 0)
    {
        coordinate = ~coordinate;
    }
    std::string output;
    while (coordinate >= 0x20)
    {
        output += (char)((0x20 | (coordinate & 0x1f)) + 63);
        coordinate >>= 5;
    }
    output += (char)(coordinate + 63);
    return output;
}
std::string encode(double current, int precision)
{
    return encode(current, 0, std::pow(10, precision));
}
std::string encode(const std::vector<PointLL>& coordinates, int precision)
{
    if (coordinates.empty())
    {
        return "";
    }

    auto factor = std::pow(10, precision);

    auto output = encode(coordinates[0].lat, 0, factor) + encode(coordinates[0].lon, 0, factor);

    for (size_t i = 1; i < coordinates.size(); i++)
    {
        auto a = coordinates[i];
        auto b = coordinates[i - 1];
        output += encode(a.lat, b.lat, factor);
        output += encode(a.lon, b.lon, factor);
    }

    return output;
};
inline unsigned char from_hex(unsigned char ch)
{
    if (ch <= '9' && ch >= '0')
        ch -= '0';
    else if (ch <= 'f' && ch >= 'a')
        ch -= 'a' - 10;
    else if (ch <= 'F' && ch >= 'A')
        ch -= 'A' - 10;
    else
        ch = 0;
    return ch;
}

std::string uri_decode(const std::string& str)
{
    std::string            result;
    std::string::size_type i;
    for (i = 0; i < str.size(); ++i)
    {
        if (str[i] == '+')
        {
            result += ' ';
        }
        else if (str[i] == '%' && str.size() > i + 2)
        {
            const unsigned char ch1 = from_hex(str[i + 1]);
            const unsigned char ch2 = from_hex(str[i + 2]);
            const unsigned char ch  = (ch1 << 4) | ch2;
            result += ch;
            i += 2;
        }
        else
        {
            result += str[i];
        }
    }
    return result;
}

} // namespace commtools