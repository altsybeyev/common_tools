#include <common_tools/algorithm.hpp>

namespace commtools
{
std::pair<std::string, std::string> find_between(const std::string& data,
                                                 const std::string& pattern1,
                                                 const std::string& pattern2, size_t& from)
{
    std::pair<std::string, std::string> result;

    auto foud = data.find(pattern1, from);

    if (foud == std::string::npos)
    {
        result.second = "not exist";
        return result;
    }

    auto foud2 = data.find(pattern2, foud);

    if (foud2 == std::string::npos)
    {
        foud2 = data.size();
    }

    size_t pos1  = foud + pattern1.size();
    result.first = data.substr(pos1, foud2 - pos1);

    from = foud2;

    return result;
}

std::pair<std::string, std::string> find_between_patterns_content(const std::string& data,
                                                                  const std::string& pattern1,
                                                                  const std::string& pattern2)
{
    size_t from = 0;
    return find_between(data, pattern1, pattern2, from);
}

std::vector<std::string> find_between_patterns_all_content(const std::string& data,
                                                           const std::string& pattern1,
                                                           const std::string& pattern2)
{
    std::vector<std::string> result;

    size_t from = 0;

    while (from != std::string::npos)
    {
        auto&& tmp = find_between(data, pattern1, pattern2, from);
        if (tmp.second.empty())
        {
            result.emplace_back(std::move(tmp.first));
        }
        else
        {
            break;
        }
    }
    return result;
}

} // namespace commtools