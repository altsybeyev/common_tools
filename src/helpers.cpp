#include <fstream>
#include <sstream>

#include <common_tools/helpers.hpp>

namespace commtools
{
std::string file_to_string(const std::string& path)
{
    std::ifstream     file(path);
    std::stringstream buffer;

    buffer << file.rdbuf();
    return buffer.str();
}
}
