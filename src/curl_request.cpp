#include <webtools/curl_request.hpp>

#include <common_tools/boostlog.hpp>

#define RETURN_ON_FAIL_MSG(cond, msg)                                                              \
    if (cond)                                                                                      \
    {                                                                                              \
        BL_ERROR() << msg;                                                                         \
        result.second = msg;                                                                       \
        return result;                                                                             \
    }

namespace webtools
{

size_t WriteCallback(char* contents, size_t size, size_t nmemb, void* userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

std::pair<std::string, std::string>
perform_curl_request(const std::string& url, const std::list<std::string>& headers,
                     const std::string& payload, const std::string& proxy,
                     const std::string& proxy_user, const std::string& method)
{
    std::pair<std::string, std::string> result;

    auto curl = curl_easy_init();
    RETURN_ON_FAIL_MSG(!curl, "error curl init")

    std::string curl_result;

    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &curl_result);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

    if (method != "")
    {
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, method.c_str());
    }

    if (!proxy.empty())
    {
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 60L);
        curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 60L);
        curl_easy_setopt(curl, CURLOPT_PROXY, proxy.c_str());
    }
    else
    {
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 15L);
        curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 4L);
    }
    if (!proxy_user.empty())
    {
        curl_easy_setopt(curl, CURLOPT_PROXYUSERPWD, proxy_user.c_str());
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
    }
    if (!headers.empty())
    {
        struct curl_slist* chunk = NULL;

        for (auto&& header : headers)
        {
            chunk = curl_slist_append(chunk, header.c_str());
        }
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
    }

    if (!payload.empty())
    {
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, payload.c_str());
    }

    auto res = curl_easy_perform(curl);
    RETURN_ON_FAIL_MSG(res != CURLE_OK,
                       "error curl perform: " + std::string(curl_easy_strerror(res)))

    BL_FTRACE() << "log_in::curl responce: " << curl_result;

    curl_easy_cleanup(curl);

    result.first = curl_result;
    return result;
}
} // namespace webtools