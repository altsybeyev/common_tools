
#ifndef COMMON_TOOLS_BOOSTLOG_TOOLS_H
#define COMMON_TOOLS_BOOSTLOG_TOOLS_H

#include <fstream>
#include <iostream>
#include <mutex>
#include <set>
#include <string>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/log/attributes/value_extraction.hpp>
#include <boost/log/attributes/value_extraction_fwd.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/expressions/formatters/char_decorator.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/phoenix/bind/bind_function.hpp>
#include <boost/thread.hpp>

#include <common_tools/boostlog.hpp>

namespace logging  = boost::log;
namespace src      = boost::log::sources;
namespace expr     = boost::log::expressions;
namespace sinks    = boost::log::sinks;
namespace attrs    = boost::log::attributes;
namespace keywords = boost::log::keywords;

namespace commtools
{
BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", sev_lvl)
BOOST_LOG_ATTRIBUTE_KEYWORD(channel, "Channel", std::string)
BOOST_LOG_ATTRIBUTE_KEYWORD(thread_id, "ThreadID",
                            logging::attributes::current_thread_id::value_type)

std::ostream& operator<<(std::ostream& strm, sev_lvl level);

void addDataLogFile(const std::string& channelIn, sev_lvl sevLvl, const std::string& attr);

std::string format_channel(logging::value_ref<std::string> const& filename);

bool check_channels(logging::value_ref<std::string> const& channel_name,
                    const std::vector<std::string>&        channels);

bool check_channel(logging::value_ref<std::string> const& channel_name, const std::string& channel);

bool my_filter(logging::value_ref<attrs::named_scope_list> const& scopes,
               std::string const&                                 target_scope);

bool check_thread(
    const std::set<thread_id_t>&                                                  thread,
    const logging::value_ref<logging::attributes::current_thread_id::value_type>& c_thread_id);

#define MAKE_SEV_FILTER(lvlTriv, sevLvl)                                                           \
    ((!expr::has_attr(severity) && logging::trivial::severity >= lvlTriv) ||                       \
     (expr::has_attr(severity) && severity >= sevLvl))

template <class T>
void set_thread_filter(const int m_lvlTriv, const sev_lvl m_sevLvl, T& sink)
{
    auto filter_loc =
        MAKE_SEV_FILTER(m_lvlTriv, m_sevLvl) &&
        (boost::phoenix::bind(
            &check_thread, sink.second,
            expr::attr<logging::attributes::current_thread_id::value_type>("ThreadID")));

    sink.first->set_filter(filter_loc);
}

#define MAKE_FORMAT()                                                                              \
    expr::stream                                                                                   \
        << "["                                                                                     \
        << expr::format_date_time<boost::posix_time::ptime>("TimeStamp", "%Y-%m-%d %H:%M:%S")      \
        << "] "                                                                                    \
        << "[" << expr::attr<boost::log::attributes::current_thread_id::value_type>("ThreadID")    \
        << "] "                                                                                    \
        << expr::if_(                                                                              \
               expr::has_attr(channel))[expr::stream << boost::phoenix::bind(                      \
                                            &format_channel, expr::attr<std::string>("Channel"))]  \
        << expr::if_(!expr::has_attr(channel))[expr::stream << "<"                                 \
                                                            << "Trivial Log"                       \
                                                            << "> "]                               \
        << expr::if_(expr::has_attr(severity))[expr::stream << severity]                           \
        << expr::if_(!expr::has_attr(                                                              \
               severity))[expr::stream << "[" << logging::trivial::severity << "] "]               \
        << expr::if_(!expr::has_attr(severity) &&                                                  \
                     logging::trivial::severity == logging::trivial::trace)[expr::stream << "  "]  \
        << expr::if_(!expr::has_attr(severity) &&                                                  \
                     logging::trivial::severity == logging::trivial::debug)[expr::stream << "  "]  \
        << expr::if_(!expr::has_attr(severity) &&                                                  \
                     logging::trivial::severity == logging::trivial::info)[expr::stream << "   "]  \
        << expr::if_(!expr::has_attr(severity) &&                                                  \
                     logging::trivial::severity == logging::trivial::warning)[expr::stream]        \
        << expr::if_(!expr::has_attr(severity) &&                                                  \
                     logging::trivial::severity == logging::trivial::error)[expr::stream << "  "]  \
        << expr::if_(!expr::has_attr(severity) &&                                                  \
                     logging::trivial::severity == logging::trivial::fatal)[expr::stream << "  "]  \
        << expr::smessage;
} // namespace commtools

#endif // COMMON_TOOLS_BOOSTLOG_TOOLS_H
