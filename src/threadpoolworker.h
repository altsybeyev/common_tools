#ifndef _THREADPOOLWORKER_H_
#define _THREADPOOLWORKER_H_

#include <queue>
#include <utility>

#include <boost/thread.hpp>

namespace commtools
{
class BaseWorker
{
  public:
    BaseWorker() : m_in_work(false), current_tag("")
    {
    }

    template <typename _Callable, typename... _Args>
    void base_init(_Callable&& __f, _Args&&... __args)
    {
        m_thread.reset(new boost::thread(std::forward<_Callable>(__f),
                                         std::forward<_Args>(__args)..., std::ref(m_in_work),
                                         std::ref(current_tag)));
    }

    boost::thread::id get_id();

    std::string get_current_tag();

    virtual ~BaseWorker();

    void join();

  protected:
    bool m_in_work;

    std::string                    current_tag;
    std::unique_ptr<boost::thread> m_thread;
};

class Worker : public BaseWorker
{
  public:
    Worker(const std::vector<std::string>& obsevers) noexcept;

    ~Worker();

    void appendFn(const std::function<void()>& fn, const std::string& tag);

    size_t getTaskCount();

    bool isEmpty();
    void wait();
    void terminate();

    void init();

  private:
    bool m_terminate;

    std::queue<std::pair<std::function<void()>, std::string>> m_fqueue;

    boost::condition_variable m_cv;

    boost::mutex m_mutex;

    const std::vector<std::string> m_observers;

    void thread_fn(bool& m_in_work, std::string& current_tag_);
};

class WorkerSharedQueue : public BaseWorker
{
  public:
    template <typename... _Args>
    void init( _Args&&... __args)
    {
        base_init(std::forward<_Args>(__args)...);
    }

    WorkerSharedQueue(const std::vector<std::string>& obsevers)
    {
    }

    void wait();
    bool isEmpty();

  private:
};

} // namespace commtools

#endif /*_THREADPOOLWORKER_H_*/
