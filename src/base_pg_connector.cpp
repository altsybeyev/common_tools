// #include <pqxx/result_iterator.hxx>

#include <base_service/base_pg_connector.hpp>
namespace base_service
{
std::string first_query_result(const q_res_t& qres)
{
    return qres.first.front().begin()->second;
}
}

// namespace base_service
// {
// std::string make_conn_string(const ConnectionInfo& conn_info)
// {
//     std::ostringstream conn_string("");
//     conn_string << "host=" << conn_info.host << " port=" << conn_info.port
//                 << " user=" << conn_info.user << " password=" << conn_info.password
//                 << " dbname=" << conn_info.db;

//     return conn_string.str();
// }
// BasePGConnector::BasePGConnector(const ConnectionInfo& conn_info)
//     : _connection(std::make_unique<pqxx::connection>(make_conn_string(conn_info)))
// {
// }

// std::list<std::map<std::string, std::string>>
// BasePGConnector::query_to_string(const pqxx::result& query_res)
// {
//     std::list<std::map<std::string, std::string>> result;
//     for (auto i = query_res.begin(), r_end = query_res.end(); i != r_end; ++i)
//     {
//         result.emplace_back();
//         auto& inseted = result.back();
//         for (auto f = i->begin(), f_end = i->end(); f != f_end; ++f)
//         {
//             inseted[f->name()] = f->c_str();
//         }
//     }
//     return result;
// }

// } // namespace base_service
