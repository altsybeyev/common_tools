#include <cmath>
#include <iostream>
#include <sstream>
#include <string>

#include <gtest/gtest.h>

#include <common_tools/service_locator.hpp>

class TestServiceLocator : public ::testing::Test
{
  protected:
    virtual void SetUp()
    {
    }
};
struct Base
{
};

struct A : Base
{
    int a;
    A() : a(1)
    {
        std::cout << "A()" << std::endl;
    }
    ~A()
    {
        std::cout << "~A()" << std::endl;
    }
};

struct B : Base
{
    int b;
    B() : b(2)
    {
        std::cout << "B()" << std::endl;
    }
    ~B()
    {
        std::cout << "~B()" << std::endl;
    }
};

TEST_F(TestServiceLocator, test1)
{
    {
        auto a = std::make_shared<A>();

        commtools::ServiceLocator<Base> locator;
        locator.set<A>(a);

        auto aa = locator.get<A>();
        EXPECT_EQ(aa->a, 1);
    }
}
