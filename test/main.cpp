#include <gtest/gtest.h>

#include <common_tools/boostlog.hpp>
#include <common_tools/threadpool.hpp>
#include <memory>


int main(int argc, char** argv)
{
    commtools::BoostLog::get(commtools::sev_lvl::debug, "test");

    commtools::ThreadPool::get(5, {"FILE"}, false);

    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
    return 0;
}
