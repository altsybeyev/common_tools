#ifndef SERREFLECTION_TEST_STRUCTURES_URL_1_H
#define SERREFLECTION_TEST_STRUCTURES_URL_1_H

#include <serreflection/defines.hpp>

enum class TestEnum
{
    en,
    ru,
};

struct TestURL_1
{
    int a;

    std::string b;

    std::vector<std::string> bb;

    TestEnum lang;
};

SERIALIZIBLE_ENUM(TestEnum, (en)(ru))

SERIALIZIBLE_STRUCT(TestURL_1, srfl::CheckModes::FATAL,
                    (int, a, 1, 0.0, 3.0)(std::string, b, "str1",
                                          INF_D())(std::vector<std::string>, bb,
                                                   DEF_D())(TestEnum, lang, "en", INF_D()))

#endif
