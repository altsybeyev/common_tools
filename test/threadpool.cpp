#include <chrono>
#include <thread>

#include <gtest/gtest.h>

#include <common_tools/boostlog.hpp>
#include <common_tools/threadpool.hpp>

using namespace std::chrono_literals;

class TestThreadPool : public ::testing::Test
{
  protected:
    virtual void SetUp()
    {
    }
};

class Sink : public commtools::BaseSink
{
  public:
    BUILD_CHILD(Sink, commtools::BaseSink)

    Sink(const Sink&) = default;

    Sink() : last_append(std::chrono::system_clock::now())
    {
    }
    std::string accumulated_log;

    std::chrono::system_clock::time_point last_append;

    void consume(logging::record_view const& rec, const std::string& command_line,
                 const std::string& tag) noexcept
    {
        accumulated_log += command_line.c_str();
        accumulated_log += "\n";
        auto now = std::chrono::system_clock::now();

        if (std::chrono::duration_cast<std::chrono::seconds>(now - last_append).count() > 1.0 ||
            accumulated_log.size() > 1)
        {
            last_append = now;
            std::cout << accumulated_log;
            accumulated_log = "";
        }
    }
};

REGISTER_CHILD_NON_TEMPLATE(Sink, commtools::BaseSink, "Sink")

TEST_F(TestThreadPool, test1)
{
    BL_FTRACE();
    commtools::ThreadPool::get().wait();

    for (int j = 0; j < 1; j++)
    {

        int n_test = 4;

        for (int i = 0; i < n_test; i++)
        {

            BL_DEBUG() << "Run iteration " << i;
            BL_DEBUG() << "Free workers: " << commtools::ThreadPool::get().free_workers();

            auto f = [=]() {
                std::this_thread::sleep_for(10ms);

                auto old_id = boost::log::aux::this_thread::get_id();

                BL_DEBUG() << "Launch async iteration with thread id = " << old_id;

                auto fut1 = commtools::ThreadPool::get().run_async_slave([=]() {
                    BL_DEBUG() << "Inner: ";
                    auto& pooll = commtools::ThreadPool::get();

                    auto fut2 = pooll.run_async_slave([=]() { BL_DEBUG() << "Inner Inner 1"; });

                    auto fut22 = pooll.run_async_slave([=]() { BL_DEBUG() << "Inner Inner 2"; });
                    fut2.get();
                    fut22.get();
                });

                fut1.get();

                BL_DEBUG() << "finish async task";
            };
            auto fut3 = commtools::ThreadPool::get().run_async_master(std::to_string(i), f);
            BL_DEBUG() << "finish async task";
        }
        BL_DEBUG() << "Finish";

        std::this_thread::sleep_for(100ms);
        commtools::ThreadPool::get().wait();
    }
    BL_DEBUG() << "Finish";
    BL_DEBUG() << "n workers: " << commtools::ThreadPool::get().size();
    BL_DEBUG() << "free workers: " << commtools::ThreadPool::get().free_workers();
}
