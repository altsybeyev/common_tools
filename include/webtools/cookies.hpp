
#ifndef COOKIES_H
#define COOKIES_H

#include <string>

namespace webtools
{
std::string get_cookie(const std::string& key, const std::string& data);
}

#endif