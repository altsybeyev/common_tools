#ifndef WEBTOOLS_DEFINITIONS_H
#define WEBTOOLS_DEFINITIONS_H

#include <map>
#include <string>

namespace webtools
{
extern std::map<std::string, std::string> countries_languages;
}

#endif