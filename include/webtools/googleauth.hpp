#ifndef GOOGLE_AUTH_H
#define GOOGLE_AUTH_H

#include <string>

#include <serreflection/defines.hpp>

namespace webtools
{

struct AuthInfoResult
{
    std::string last_name;
    std::string email;
    std::string given_name;
    std::string family_name;
    std::string iss;
    int         exp;
    std::string aud;
    std::string login_as;
    std::string picture;
};

std::pair<std::unique_ptr<AuthInfoResult>, std::string>
check_get_google_auth(const std::string& tocken, const std::map<std::string, std::string>& ids,
                      const std::string& host);

} // namespace webtools

SERIALIZIBLE_STRUCT(webtools::AuthInfoResult, srfl::CheckModes::FATAL,
                    (std::string, family_name, "", "", "")(std::string, given_name, "", "",
                                                           "")(std::string, email,
                                                               DEF_D())(std::string, iss, DEF_D())(
                        std::string, aud, DEF_D())(int, exp, DEF_D())(std::string, picture, "", "",
                                                                      ""))

#endif