
#ifndef CURL_REQUEST_H
#define CURL_REQUEST_H

#include <curl/curl.h>
#include <list>
#include <string>

namespace webtools
{
std::pair<std::string, std::string>
perform_curl_request(const std::string& url, const std::list<std::string>& headers = {},
                     const std::string& payload = "", const std::string& proxy = "",
                     const std::string& proxy_user = "", const std::string& method = "");
}

#endif