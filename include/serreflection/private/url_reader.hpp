#ifndef SERREFLECTION_URL_READER_H
#define SERREFLECTION_URL_READER_H

#include <common_tools/algorithm.hpp>
#include <serreflection/private/base_reader.hpp>

namespace srfl
{

namespace
{

void convert(const std::string& in, int& out)
{
    out = std::stoi(in);
}

void convert(const std::string& in, double& out)
{
    out = std::stod(in);
}

void convert(const std::string& in, bool& out)
{
    out = (in == "true" || in == "True");
}

void convert(const std::string& in, std::string& out)
{
    out = in;
}
} // namespace

template <typename T>
class URLReader : public BaseReader<T, std::string, URLReader<T>>
{
  public:
    template <typename Tstring>
    URLReader(Tstring&& pt) : BaseReader<T, std::string, URLReader<T>>(std::forward<Tstring>(pt))
    {
    }

    template <typename Tstring>
    URLReader(Tstring&& pt, T& object)
        : BaseReader<T, std::string, URLReader<T>>(std::forward<Tstring>(pt), object)
    {
    }

    template <typename Targ>
    bool read_optional_concrete(Targ& element, const std::string& name,
                                const bool is_fundamental) const
    {
        const std::string& data = this->m_data;

        const auto res = commtools::find_between_patterns_content(data, name + "=", "&");

        if (!res.second.empty() || res.first.empty())
        {
            throw std::runtime_error(res.second);
        }

        convert(commtools::uri_decode(res.first), element);

        return true;
    }

    template <typename Tvector>
    void read_vector(std::vector<Tvector>& element, const std::string& name) const
    {
        const std::string& data = this->m_data;

        const auto res = commtools::find_between_patterns_all_content(data, name + "=", "&");

        element = res;

        // if (res.second.empty() && !res.first.empty())
        // {
        //     element = commtools::strsplit_adapter<std::vector<Tvector>>(
        //         commtools::uri_decode(res.first), ",");
        // }
    }

    template <typename Tvector>
    void read_fundamental_vector(std::vector<Tvector>& element, const std::string& name) const
    {
        read_vector(element, name);
    }

    template <typename Tvector>
    void read_objects_vector(std::vector<Tvector>& element, const std::string& name) const
    {
        read_vector(element, name);
    }

    void read_string(std::string& element, const std::string& name) const
    {
        this->read_optional(element, name);
    }

    template <typename Targ>
    void read_fundamental(Targ& element, const std::string& name) const
    {
        this->read_optional(element, name);
    }

    template <typename Targ>
    void read_class(Targ& element, const std::string& name) const
    {
    }
};
} // namespace srfl

#endif
