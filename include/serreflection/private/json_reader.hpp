#ifndef SERREFLECTION_JSON_READER_H
#define SERREFLECTION_JSON_READER_H

#include <serreflection/private/base_reader.hpp>

namespace srfl
{
template <typename T, typename Tpt>
void read_json_p(Tpt&& pt, T& object);

template <typename T>
class JSONReader : public BaseReader<T, boost::property_tree::ptree, JSONReader<T>>
{
  public:
    template <typename Tpt>
    JSONReader(Tpt&& pt)
        : BaseReader<T, boost::property_tree::ptree, JSONReader<T>>(std::forward<Tpt>(pt))
    {
    }

    template <typename Tpt>
    JSONReader(Tpt&& pt, T& object)
        : BaseReader<T, boost::property_tree::ptree, JSONReader<T>>(std::forward<Tpt>(pt), object)
    {
    }

    template <typename Targ>
    bool read_optional_concrete(Targ& element, const std::string& name,
                                const bool is_fundamental) const
    {
        const boost::property_tree::ptree& pt = this->m_data;

        if (is_fundamental)
        {
            element = pt.get<Targ>(name);
        }
        else
        {
            pt.get_child(name);
        }
        return true;
    }

    template <typename Tvector>
    void read_fundamental_vector(std::vector<Tvector>& element, const std::string& name) const
    {
        const boost::property_tree::ptree& pt = this->m_data;

        Tvector default_val;

        if (!this->read_optional(default_val, name, false))
        {
            element.resize(1);
            element[0] = default_val;
            return;
        }

        for (auto& item : pt.get_child(name))
        {
            boost::property_tree::ptree tmp = item.second;
            element.push_back(tmp.get<Tvector>(""));
        }
    }

    template <typename Tvector>
    void read_objects_vector(std::vector<Tvector>& element, const std::string& name) const
    {
        const boost::property_tree::ptree& pt = this->m_data;

        for (auto& item : pt.get_child(name))
        {
            boost::property_tree::ptree tmp = item.second;
            element.emplace_back();
            auto& rf = element.back();
            read_json_p(std::move(tmp), rf);
        }
    }

    void read_string(std::string& element, const std::string& name) const
    {
        this->read_optional(element, name);
    }

    template <typename Targ>
    void read_fundamental(Targ& element, const std::string& name) const
    {
        this->read_optional(element, name);
    }

    template <typename Targ>
    void read_class(Targ& element, const std::string& name) const
    {
        const boost::property_tree::ptree& pt = this->m_data;
        read_json_p<Targ>(std::move(pt.get_child(name)), element);
    }
};
} // namespace srfl

#endif
