#ifndef SERREFLECTION_BASE_READER_H
#define SERREFLECTION_BASE_READER_H

#include <cmath>
#include <memory>

#include <boost/fusion/algorithm.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/for_each.hpp>
#include <boost/fusion/include/size.hpp>
#include <boost/fusion/tuple.hpp>

#include <boost/mpl/range_c.hpp>

#include <serreflection/private/defines_private.hpp>

namespace srfl
{

namespace
{
template <class T>
bool check_default(T val, typename std::enable_if<std::is_fundamental<T>::value>::type* = 0)
{
    return std::isnan(val);
}

bool check_default(const std::string& val)
{
    if ("NaN" == val)
    {
        return true;
    }
    return false;
}
} // namespace

template <typename T, typename Tdata, typename Base>
class BaseReader
{
  public:
    template <typename TTdata>
    BaseReader(TTdata&& data, T& value) : m_value(&value), m_data(std::forward<TTdata>(data))
    {
    }

    template <typename Targ>
    void operator()(const Targ& x) const
    {
        constexpr size_t index = Targ::value;

        read(boost::fusion::at_c<index>(*m_value),
             boost::fusion::extension::struct_member_name<T, index>::call());
    }

  protected:
    template <typename Targ>
    bool read_optional(Targ& element, const std::string& name,
                       const bool is_fundamental = true) const
    {
        try
        {
            return static_cast<const Base*>(this)->read_optional_concrete(element, name,
                                                                          is_fundamental);
        }
        catch (const std::exception& ex)
        {
            auto msg = "Failed to read variable: " + srfl::StructChecker<T>::class_name +
                       "::" + name + " : " + ex.what();

            auto default_val = srfl::StructChecker<T>::get_default_value(name, element);

            if (check_default(default_val))
            {
                throw std::runtime_error(msg);
            }

            BL_WARNING() << msg << ". Use default value: " << default_val;

            element = default_val;
            return false;
        }
    }

    void read(bool& element, const std::string& name) const
    {
        static_cast<const Base*>(this)->read_fundamental(element, name);
    }

    template <typename Targ>
    void read(Targ& element, const std::string& name,
              typename std::enable_if<std::is_fundamental<Targ>::value>::type* = 0) const
    {
        static_cast<const Base*>(this)->read_fundamental(element, name);
        srfl::StructChecker<T>::check_value(element, name);
    }

    template <typename Targ>
    void read_enum(Targ& element, const std::string& name) const
    {
        std::string value;

        if (!this->read_optional(value, name))
        {
            return;
        }

        // auto default_val = srfl::StructChecker<T>::get_default_value(name, element);

        auto it = srfl::EnumParser<Targ>::parse_map.find(value);

        if (srfl::EnumParser<Targ>::parse_map.end() == it)
        {
            auto msg = "Failed to read variable: " + srfl::StructChecker<T>::class_name +
                       "::" + name + " : unexpected value \"" + value + "\"";

            throw std::runtime_error(msg);
        }
        else
        {
            element = it->second;
        }
    }

    template <typename Targ>
    void read(Targ& element, const std::string& name,
              typename std::enable_if<std::is_enum<Targ>::value>::type* = 0) const
    {
        read_enum(element, name);
    }

    template <typename Targ>
    void read(Targ& element, const std::string& name,
              typename std::enable_if<std::is_class<Targ>::value>::type* = 0) const
    {
        static_cast<const Base*>(this)->read_class(element, name);
    }

    template <typename Tvector>
    void read(std::vector<Tvector>& element, const std::string& name,
              typename std::enable_if<std::is_fundamental<Tvector>::value>::type* = 0) const
    {
        element.clear();

        static_cast<const Base*>(this)->read_fundamental_vector(element, name);
        srfl::StructChecker<T>::check_value(element, name);
    }

    template <typename Tvector>
    void read(std::vector<Tvector>& element, const std::string& name,
              typename std::enable_if<std::is_class<Tvector>::value>::type* = 0) const
    {
        element.clear();
        static_cast<const Base*>(this)->read_objects_vector(element, name);
    }

    void read(std::string& element, const std::string& name) const
    {
        static_cast<const Base*>(this)->read_string(element, name);
    }
    T* m_value; // pointer to serializible object

    Tdata m_data; // source of serialization data

  private:
};
} // namespace srfl

#endif
