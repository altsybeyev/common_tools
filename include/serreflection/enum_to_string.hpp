#ifndef SERREFLECTION_ENUM_TO_STRING_H
#define SERREFLECTION_ENUM_TO_STRING_H

#include <serreflection/private/defines_private.hpp>

namespace srfl
{
template <class T>
std::string enum_to_string(const T& val)
{
    return srfl::EnumParser<T>::inv_parse_map.at(val);
}
} // namespace srfl

#endif
