#ifndef SERREFLECTION_READ_URL_H
#define SERREFLECTION_READ_URL_H

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <common_tools/boostlog.hpp>

#include <serreflection/private/read_url_private.hpp>
#include <serreflection/private/defines_private.hpp>

namespace srfl
{
template <typename T>
bool read_url(const std::string& data, T& object) noexcept
{
    try
    {
        read_url_p(data, object);
        return true;
    }
    catch (const std::exception& ex)
    {
        BL_ERROR() << ex.what();
        return false;
    }
}

template <typename T>
std::unique_ptr<T> read_url(const std::string& data) noexcept
{
    auto result = std::make_unique<T>();
    if(is_empty<T>::value)
    {
        return result;
    }
    if (read_url(data, *result))
    {
        return result;
    }
    return nullptr;
}

template <class T>
std::unique_ptr<T> read_url_string(const std::string& data) noexcept
{
    return read_url<T>(data);
}
}

#endif
