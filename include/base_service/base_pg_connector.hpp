#ifndef COMM_TOOLS_BASE_PG_CONNECTOR_HPP
#define COMM_TOOLS_BASE_PG_CONNECTOR_HPP

#include <condition_variable>
#include <memory>
#include <queue>

#include <experimental/propagate_const>

#include <pqxx/connection>
#include <pqxx/result_iterator.hxx>
#include <pqxx/transaction>

#include <common_tools/base_singletone_instance.hpp>
#include <common_tools/query_former.hpp>

#include <base_service/conninfo.hpp>

namespace base_service
{
using q_res_t = std::pair<std::list<std::map<std::string, std::string>>, std::string>;

std::string first_query_result(const q_res_t& qres);

template <class T>
class BasePGConnector : public commtools::BaseSingletone<T>
{
  public:
    explicit BasePGConnector(const ConnectionInfo& conn_info)
        : conn_string_(make_conn_string(conn_info))
    {
        make_pool(conn_info.n_connections);
    }

    virtual ~BasePGConnector() = default;

    q_res_t make_query(const std::string& query) noexcept
    {
        q_res_t result;

        auto connection = get_connection();

        try
        {
            if (!connection)
            {
                connection = std::make_unique<pqxx::connection>(conn_string_);
            }

            pqxx::work xact(*connection);

            result = std::make_pair(query_to_string(xact.exec(query)), "");

            xact.commit();
        }
        catch (const std::exception& ex)
        {
            BL_ERROR() << "Failed to execute query: " << query << " : " << ex.what();
            result = std::make_pair(std::list<std::map<std::string, std::string>>{}, ex.what());
        }

        free_connection(connection);
        return result;
    }

    template <class... Args>
    q_res_t make_query(const std::string& f_name, const Args&... args) noexcept
    {

        return make_query(commtools::QueryFormer::make_func_query(f_name, args...));
    }

  protected:
    static std::list<std::map<std::string, std::string>>
    query_to_string(const pqxx::result& query_res)
    {
        std::list<std::map<std::string, std::string>> result;
        for (auto i = query_res.begin(), r_end = query_res.end(); i != r_end; ++i)
        {
            result.emplace_back();
            auto& inseted = result.back();
            for (auto f = i->begin(), f_end = i->end(); f != f_end; ++f)
            {
                inseted[f->name()] = f->c_str();
            }
        }
        return result;
    }

  private:
    std::shared_ptr<pqxx::connection> get_connection() noexcept
    {
        std::unique_lock<std::mutex> lock_(mutex_);

        while (_connections.empty())
        {
            _condition.wait(lock_);
        }

        auto conn_ = _connections.front();
        _connections.pop();

        return conn_;
    }

    void free_connection(std::shared_ptr<pqxx::connection> conn_) noexcept
    {
        std::unique_lock<std::mutex> lock_(mutex_);
        _connections.push(conn_);
        lock_.unlock();
        _condition.notify_one();
    }

    void make_pool(const size_t pool_size)
    {
        std::lock_guard<std::mutex> locker_(mutex_);

        for (auto i = 0; i < pool_size; ++i)
        {
            _connections.emplace(nullptr);
        }
    }

    std::mutex  mutex_;
    std::string conn_string_;

    std::condition_variable _condition;

    std::queue<std::shared_ptr<pqxx::connection>> _connections;

    static std::string make_conn_string(const ConnectionInfo& conn_info)
    {
        std::ostringstream conn_string("");
        conn_string << "host=" << conn_info.host << " port=" << conn_info.port
                    << " user=" << conn_info.user << " dbname=" << conn_info.db;

        if (!conn_info.password.empty())
        {
            conn_string << " password=" << conn_info.password;
        }

        BL_DEBUG() << "Conn string: " << conn_string.str();

        return conn_string.str();
    }
};

} // namespace base_service

#endif
