#ifndef COMM_TOOLS_BASE_CONFIG_HPP
#define COMM_TOOLS_BASE_CONFIG_HPP

#include <common_tools/boostlog.hpp>
#include <serreflection/defines.hpp>

#include <base_service/conninfo.hpp>

namespace base_service
{
template <class Tserv>
struct BaseServiceConfig
{
    commtools::BoostLogConfig log;

    ConnectionInfo db;

    Tserv service;
};

template <class Tserv>
class ServiceConfig : public commtools::BaseSingletone<Tserv>
{
};
} // namespace base_service

SERIALIZIBLE_STRUCT(commtools::BoostLogConfig, srfl::CheckModes::FATAL,
                    (std::string, level, DEF_D())(std::string, path,
                                                  DEF_D())(std::string, general_file, DEF_D()))

#define SER_BASE(Tserv)                                                                            \
    SERIALIZIBLE_STRUCT(base_service::BaseServiceConfig<Tserv>, srfl::CheckModes::FATAL,           \
                        (commtools::BoostLogConfig, log, DEF_D())(                                 \
                            base_service::ConnectionInfo, db, DEF_D())(Tserv, service, DEF_D()))

#endif
