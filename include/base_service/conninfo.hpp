#ifndef COMM_TOOLS_CONNINFO_HPP
#define COMM_TOOLS_CONNINFO_HPP

#include <string>

#include <serreflection/defines.hpp>

namespace base_service
{
struct ConnectionInfo
{
    std::string host;
    std::string port;
    std::string db;
    std::string user;
    std::string password;
    int         n_connections;
};
} // namespace base_service

SERIALIZIBLE_STRUCT(base_service::ConnectionInfo, srfl::CheckModes::FATAL,
                    (std::string, host, DEF_D())(std::string, port, DEF_D())(std::string, db,
                                                                             DEF_D())(
                        std::string, user, DEF_D())(std::string, password,
                                                    DEF_D())(int, n_connections, srfl::nan, 1,
                                                             srfl::inf))

#endif
