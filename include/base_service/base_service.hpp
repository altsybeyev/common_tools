#ifndef COMM_TOOLS_BASE_SERVICE_HPP
#define COMM_TOOLS_BASE_SERVICE_HPP

#include <csignal>
#include <exception>
#include <memory>

#include <serreflection/read_json.hpp>

#include <common_tools/boostlog.hpp>
#include <common_tools/helpers.hpp>

#include <base_service/baseconfig.hpp>

namespace base_service
{

template <class Tconfig, class TService, class TConnector>
class BaseServiceInitializer
{
  public:
    BaseServiceInitializer(int argc, char** argv)
    {
        setup_signals();
        if (argc != 2)
        {
            throw std::invalid_argument("Incorrect number of input arguments");
        }
        auto config = srfl::read_json_string<base_service::BaseServiceConfig<Tconfig>>(
            commtools::file_to_string(argv[1]));
        if (!config)
        {
            throw std::invalid_argument("Invalid config");
        }

        init_boostlog(config->log);
        TConnector::get(config->db);
        ServiceConfig<Tconfig>::get(config->service);

        TService service(config->service);
        service.run();
    }

  protected:
    virtual void init_boostlog(const commtools::BoostLogConfig& config)
    {
        const std::vector<std::pair<std::string, std::string>> dummy = {};
        commtools::BoostLog::get(config, dummy, true);
    }

    virtual void setup_signals()
    {
        std::signal(SIGHUP, sighandler);
        std::signal(SIGINT, sighandler);
        std::signal(SIGQUIT, sighandler);
        std::signal(SIGILL, sighandler);
        std::signal(SIGABRT, sighandler);
        std::signal(SIGFPE, sighandler);
        std::signal(SIGSEGV, sighandler);
        std::signal(SIGALRM, sighandler);
        std::signal(SIGTERM, sighandler);
        std::signal(SIGPIPE, SIG_IGN);
    }
    static void sighandler(int signum)
    {
        if (signum == SIGHUP)
        {
            commtools::BoostLog::get().rotate_general_file();
        }
        else
        {
            exit(1);
        }
    }
};

template <class Tconfig>
class BaseService
{
  public:
    explicit BaseService(const Tconfig& config) : _config(config)
    {
    }

  protected:
    const Tconfig _config;
};

} // namespace base_service

#endif
