#ifndef COMMON_TOOLS_PG_HPP
#define COMMON_TOOLS_PG_HPP

#include <string>

namespace commtools
{
std::string safe_pg_string(const std::string& input);
std::string safe_pg_string_single_quot(const std::string& input);
}

#endif // COMMON_TOOLS_PG_HPP
