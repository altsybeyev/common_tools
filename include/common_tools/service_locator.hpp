#ifndef COMMON_TOOLS_SERVICE_LOCATOR_HPP
#define COMMON_TOOLS_SERVICE_LOCATOR_HPP

#include <map>
#include <memory>
#include <type_traits>

namespace commtools
{

class ServiceLocatorKey
{
  public:
    template <class... Args>
    static ServiceLocatorKey form_key(const Args... args)
    {
        ServiceLocatorKey out;

        setKey(out, args...);

        return out;
    }

    template <class T, class... Args>
    static ServiceLocatorKey form_key(const Args... args)
    {
        return form_key(typeid(T).hash_code(), args...);
    }

    friend bool operator<(const ServiceLocatorKey& lhs, const ServiceLocatorKey& rhs)
    {
        return lhs.m_data < rhs.m_data;
    }

  private:
    std::string m_data;

    static void setKey(ServiceLocatorKey& key)
    {
        key.m_data += "!";
    }

    template <class T, class... Args>
    static void setKey(ServiceLocatorKey& key, const T& first, const Args... args)
    {
        key.m_data += "!_" + getString(first);
        setKey(key, args...);
    }

    template <class T>
    static typename std::enable_if<std::is_arithmetic<T>::value, std::string>::type
    getString(const T& input)
    {
        return std::to_string(input);
    }

    template <class T>
    static typename std::enable_if<!std::is_arithmetic<T>::value, std::string>::type
    getString(const T& input)
    {
        return std::string(input);
    }
};

template <class TBase>
class ServiceLocator
{
  public:
    ServiceLocator()          = default;
    virtual ~ServiceLocator() = default;

    template <class... Keys>
    std::shared_ptr<TBase> get_base_code(const size_t code, const Keys... keys) const
    {
        auto it = m_store.find(ServiceLocatorKey::form_key(code, keys...));

        if (it != std::end(m_store))
        {
            return it->second;
        }
        return nullptr;
    }

    template <class T, class... Keys>
    std::shared_ptr<TBase> get_base(const Keys... keys) const
    {
        static_assert(std::is_base_of<TBase, T>::value, "service must be base on TBase");

        return get_base_code(typeid(T).hash_code(), keys...);
    }

    template <class T, class... Keys>
    std::shared_ptr<T> get(const Keys... keys) const
    {
        return std::static_pointer_cast<T>(get_base<T>(keys...));
    }

    template <class T, class... Keys>
    void set(const std::shared_ptr<T>& service, const Keys... keys)
    {
        static_assert(std::is_base_of<TBase, T>::value, "service must be base on TBase");

        m_store[ServiceLocatorKey::form_key(typeid(T).hash_code(), keys...)] =
            std::static_pointer_cast<TBase>(service);
    }

    template <class... Keys>
    void set_code(const std::shared_ptr<TBase>& service, const size_t code, const Keys... keys)
    {
        m_store[ServiceLocatorKey::form_key(code, keys...)] = service;
    }

    size_t size() const noexcept
    {
        return m_store.size();
    }

  private:
    std::map<ServiceLocatorKey, std::shared_ptr<TBase>> m_store;
};
}

#endif
