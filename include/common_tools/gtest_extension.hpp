
#ifndef COMMON_GTEST_EXTENSION_H
#define COMMON_GTEST_EXTENSION_H

#define MOCK_SPEC_METHOD0(spec, m, ...) GMOCK_METHOD0_(, spec, , m, __VA_ARGS__)
#define MOCK_SPEC_METHOD1(spec, m, ...) GMOCK_METHOD1_(, spec, , m, __VA_ARGS__)
#define MOCK_SPEC_METHOD2(spec, m, ...) GMOCK_METHOD2_(, spec, , m, __VA_ARGS__)
#define MOCK_SPEC_METHOD3(spec, m, ...) GMOCK_METHOD3_(, spec, , m, __VA_ARGS__)
#define MOCK_SPEC_METHOD4(spec, m, ...) GMOCK_METHOD4_(, spec, , m, __VA_ARGS__)
#define MOCK_SPEC_METHOD5(spec, m, ...) GMOCK_METHOD5_(, spec, , m, __VA_ARGS__)
#define MOCK_SPEC_METHOD6(spec, m, ...) GMOCK_METHOD6_(, spec, , m, __VA_ARGS__)
#define MOCK_SPEC_METHOD7(spec, m, ...) GMOCK_METHOD7_(, spec, , m, __VA_ARGS__)
#define MOCK_SPEC_METHOD8(spec, m, ...) GMOCK_METHOD8_(, spec, , m, __VA_ARGS__)
#define MOCK_SPEC_METHOD9(spec, m, ...) GMOCK_METHOD9_(, spec, , m, __VA_ARGS__)

#endif // COMMON_GTEST_EXTENSION_H
