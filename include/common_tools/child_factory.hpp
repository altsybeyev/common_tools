#ifndef COMM_TOOLS_CHILD_FACTORY_HPP
#define COMM_TOOLS_CHILD_FACTORY_HPP

#include <map>
#include <memory>

// How to use this factory:
// struct Base
// {
//     BUILD_CHILD_FACTORY(std::string, Base)
// };

// struct Der : Base
// {
//     BUILD_CHILD(Der, Base)
// };

// REGISTER_CHILD_NON_TEMPLATE(Der, Base, "Der")

// auto der = Base::make_child("Der");

#define COMMA ,

#define BUILD_CHILD_FACTORY(TKey, TParent)                                                         \
    struct ChildFactory                                                                            \
    {                                                                                              \
        static ChildFactory& instance() noexcept                                                   \
        {                                                                                          \
            static ChildFactory instance;                                                          \
            return instance;                                                                       \
        }                                                                                          \
        bool add(const TKey& key, TParent* child_ptr) noexcept                                     \
        {                                                                                          \
            map[key] = child_ptr;                                                                  \
            return true;                                                                           \
        }                                                                                          \
        std::shared_ptr<TParent> make_child(const TKey& key) const noexcept                        \
        {                                                                                          \
            auto it = map.find(key);                                                               \
            if (it == map.end())                                                                   \
            {                                                                                      \
                return nullptr;                                                                    \
            }                                                                                      \
            return it->second->clone();                                                            \
        }                                                                                          \
        std::unique_ptr<TParent> make_unique_child(const TKey& key) const noexcept                 \
        {                                                                                          \
            auto it = map.find(key);                                                               \
            if (it == map.end())                                                                   \
            {                                                                                      \
                return nullptr;                                                                    \
            }                                                                                      \
            return it->second->clone_unique();                                                     \
        }                                                                                          \
                                                                                                   \
        std::map<TKey, TParent*> map;                                                              \
    };                                                                                             \
    static std::shared_ptr<TParent> make_child(const TKey& key) noexcept                           \
    {                                                                                              \
        return ChildFactory::instance().make_child(key);                                           \
    }                                                                                              \
    static std::unique_ptr<TParent> make_unique_child(const TKey& key) noexcept                    \
    {                                                                                              \
        return ChildFactory::instance().make_unique_child(key);                                    \
    }                                                                                              \
    virtual TKey                     get_key() const noexcept      = 0;                            \
    virtual std::shared_ptr<TParent> clone() const noexcept        = 0;                            \
    virtual std::unique_ptr<TParent> clone_unique() const noexcept = 0;                            \
    const static TKey                key;

#define BUILD_CHILD(T, TParent)                                                                    \
    virtual std::shared_ptr<TParent> clone() const noexcept override final                         \
    {                                                                                              \
        return std::make_shared<T>(*this);                                                         \
    }                                                                                              \
    virtual std::unique_ptr<TParent> clone_unique() const noexcept override final                  \
    {                                                                                              \
        return std::make_unique<T>(*this);                                                         \
    }                                                                                              \
                                                                                                   \
    virtual typename std::remove_const<decltype(TParent::key)>::type get_key()                     \
        const noexcept override                                      final                         \
    {                                                                                              \
        return key;                                                                                \
    }                                                                                              \
    const static decltype(TParent::key) key;                                                       \
    const static bool                   registered;

#define REGISTER_CHILD(T, TParent, key_value)                                                      \
    template <>                                                                                    \
    const bool T::registered = TParent::ChildFactory::instance().add(key_value, new T());          \
    template <>                                                                                    \
    const decltype(TParent::key) T::key = key_value;

#define REGISTER_CHILD_NON_TEMPLATE(T, TParent, key_value)                                           \
    const bool T::registered            = TParent::ChildFactory::instance().add(key_value, new T()); \
    const decltype(TParent::key) T::key = key_value;

#endif
