// helper for sql query formation
#ifndef COMM_TOOLS_QUERY_FORMER__HPP
#define COMM_TOOLS_QUERY_FORMER__HPP

#include <iomanip>
#include <iostream>

#include <common_tools/pg.hpp>
#include <webtools/googleauth.hpp>

namespace commtools
{
class QueryFormer
{
  public:
    static std::string make_func_query(const std::string& func_name) noexcept
    {
        return "SELECT * FROM " + func_name + "()";
    }
    template <class... Args>
    static std::string make_func_query(const std::string& func_name, const Args&... args) noexcept
    {
        std::string result = "";
        auto        inner  = make_query_p(result, args...);

        if (inner.size() > 1)
        {
            inner.pop_back();
            inner.pop_back();
        }

        return "SELECT * FROM " + func_name + "(" + inner + ")";
    }

  private:
    template <class T>
    static typename std::enable_if<std::is_arithmetic<T>::value, std::string>::type
    get_string(const T& arg) noexcept
    {
        std::stringstream str;
        str << std::setprecision(16) << arg;
        return str.str();
    }

    template <class T>
    static typename std::enable_if<!std::is_arithmetic<T>::value, std::string>::type
    get_string(const T& arg) noexcept
    {
        return "'" + std::string(arg) + "'";
    }

    static std::string get_string(const bool& arg) noexcept
    {
        return arg ? "true" : "false";
    }

    static std::string get_string(const std::string& arg) noexcept
    {
        return "'" + safe_pg_string_single_quot(arg) + "'";
    }

    static std::string get_string(const std::unique_ptr<webtools::AuthInfoResult>& auth) noexcept
    {
        return get_string(safe_pg_string(auth->given_name)) + ", " +
               get_string(safe_pg_string(auth->family_name)) + ", " + get_string(auth->email);
    }

    static std::string get_string(const std::vector<std::string>& arg) noexcept
    {
        std::stringstream str;

        if (arg.empty() || (arg.size() == 1 && arg[0].empty()))
        {
            str << "'{}'";
            return str.str();
        }

        str << "'{";
        for (const auto& v : arg)
        {
            str << "\"" << v << "\""
                << ",";
        }

        auto result = str.str();
        result.pop_back();
        result += "}'";

        return result;
    }

    static std::string get_string(const std::vector<std::vector<float>>& arg) noexcept
    {
        std::stringstream str;

        if (arg.empty() || (arg.size() == 1 && arg[0].empty()))
        {
            str << "'{}'";
            return str.str();
        }

        str << std::setprecision(16) << "'{";
        for (const auto& v : arg)
        {
            str << "{";
            for (size_t i = 0; i < v.size(); i++)
            {
                str << v[i];
                if (i < v.size() - 1)
                {
                    str << ",";
                }
            }
            str << "},";
        }
        auto result = str.str();
        result.pop_back();
        result += "}'";

        return result;
    }

    static void check_get_arg(std::string& result, const std::string& arg)
    {
        if (arg != "{}")
        {
            result += get_string(arg) + ", ";
        }
    }

    static void check_get_arg(std::string&                                     result,
                              const std::unique_ptr<webtools::AuthInfoResult>& arg)
    {
        if (arg)
        {
            result += get_string(arg) + ", ";
        }
    }

    static void check_get_arg(std::string& result, const std::unique_ptr<std::string>& arg)
    {
        if (arg)
        {
            result += *arg + ", ";
        }
    }

    template <class T>
    static void check_get_arg(std::string& result, const T& arg)
    {
        result += get_string(arg) + ", ";
    }

    template <class T, class... Args>
    static std::string make_query_p(const std::string& in, T& arg, const Args&... args) noexcept
    {
        std::string result = in;
        check_get_arg(result, arg);
        return make_query_p(result, args...);
    }

    template <class T, class... Args>
    static std::string make_query_p(const std::string& in, const T& arg,
                                    const Args&... args) noexcept
    {
        std::string result = in;
        check_get_arg(result, arg);
        return make_query_p(result, args...);
    }

    template <class... Args>
    static std::string make_query_p(const std::string& in) noexcept
    {
        return in;
    }
};
} // namespace commtools
#endif
