#ifndef BASE_SINGLETONE_H
#define BASE_SINGLETONE_H

namespace commtools
{
template <class T>
class BaseSingletone
{
  public:
    template <class... Args>
    static T& get(const Args&... args)
    {
        if (!_instance)
        {
            _instance = new T(args...);
        }
        return *_instance;
    }

    static T& get()
    {
        if (!_instance)
        {
            std::terminate();
        }
        return *_instance;
    }

  protected:
    static T* _instance;
};
} // namespace commtools

#define INIT_SINGLETONE(Type)                                                                      \
    template <>                                                                                    \
    Type* commtools::BaseSingletone<Type>::_instance = nullptr;

#endif
