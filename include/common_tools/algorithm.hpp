
#ifndef COMMON_TOOLS_ALGORITHM_H
#define COMMON_TOOLS_ALGORITHM_H

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/regex.hpp>
#include <boost/regex.hpp>

namespace commtools
{
template <class T>
T strsplit_adapter(const std::string& data, const std::string& split, const bool is_any_of = true)
{
    T result;

    if (is_any_of)
    {
        boost::algorithm::split(result, data, boost::is_any_of(split));
    }
    else
    {
        boost::algorithm::split_regex(result, data, boost::regex(split));
    }

    result.erase(
        std::remove_if(result.begin(), result.end(), [](const auto& val) { return val.empty(); }),
        result.end());

    return result;
}

struct PointLL
{
    double lat;
    double lon;
    PointLL(const double la, const double lo);
};

std::vector<PointLL> decode(const std::string& encoded, int precision);
std::string          encode(const std::vector<PointLL>& coordinates, int precision);
std::string          encode(double current, int precision);

std::pair<std::string, std::string> find_between_patterns_content(const std::string& data,
                                                                  const std::string& pattern1,
                                                                  const std::string& pattern2);

std::vector<std::string> find_between_patterns_all_content(const std::string& data,
                                                           const std::string& pattern1,
                                                           const std::string& pattern2);

std::string uri_decode(const std::string& sSrc);

} // namespace commtools

#endif
