#ifndef BOOSTLOG0_H
#define BOOSTLOG0_H

#include <map>
#include <mutex>
#include <set>
#include <vector>

#include <boost/log/detail/thread_id.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/sources/severity_channel_logger.hpp>
#include <boost/log/trivial.hpp>

#include <common_tools/base_singletone_instance.hpp>
#include <common_tools/child_factory.hpp>

#define BL_VAL(str) #str
#define BL_TOSTRING(str) BL_VAL(str)

namespace sinks   = boost::log::sinks;
namespace logging = boost::log;

namespace commtools
{

enum class sev_lvl
{
    trace,
    debug,
    info,
    warning,
    error,
    critical
};

typedef boost::log::sources::severity_channel_logger_mt<sev_lvl, std::string>  sev_ch_log;
typedef boost::log::v2_mt_posix::aux::id<boost::log::v2_mt_posix::aux::thread> thread_id_t;
typedef boost::log::v2_mt_posix::sinks::synchronous_sink<
    boost::log::v2_mt_posix::sinks::text_file_backend>
    sink_t;

struct BoostLogConfig
{
    std::string level;
    std::string path;
    std::string general_file;
};

class BaseSink
{
  public:
    BUILD_CHILD_FACTORY(std::string, BaseSink)
    virtual void consume(logging::record_view const& rec, const std::string& command_line,
                         const std::string& tag) noexcept = 0;
};

class CustomSink : public sinks::basic_formatted_sink_backend<char, sinks::synchronized_feeding>
{
  public:
    CustomSink(const std::string& type, const std::string& tag)
        : sink(BaseSink::make_unique_child(type)), m_tag(tag)

    {
    }

    void consume(logging::record_view const& rec, string_type const& command_line)
    {
        sink->consume(rec, command_line, m_tag);
    }
    void set_tag(const std::string& tag) noexcept;

  private:
    const std::unique_ptr<BaseSink> sink;

    const std::string m_tag;
};

typedef sinks::synchronous_sink<CustomSink> sink_custom_t;

class BoostLog : public BaseSingletone<BoostLog>
{
    friend class BaseSingletone;

  public:
    void add_current_thread_filter(const std::string& tag, const std::string& type);
    void remove_current_thread_filter(const std::string& tag, const std::string& type);
    void rotate_general_file();

    template <class Tmap, class... Args>
    void add_current_thread_filter_common(Tmap& map, const std::string& tag, const Args&... args);

    template <class Tmap>
    void remove_current_thread_filter_common(Tmap& map, const std::string& tag);

    const static std::vector<std::string> m_sev_strings;

    const static int rot_size;

  private:
    std::mutex m_add_thread_file_mitex;

    boost::shared_ptr<sink_t> m_g_file_sink;

    sev_lvl m_sevLvl;

    decltype(boost::log::trivial::trace) m_lvlTriv;

    std::string m_fmtGeneral;

    std::string m_log_path;

    std::map<std::string, std::pair<boost::shared_ptr<sink_t>, std::set<thread_id_t>>> m_files_map;
    std::map<std::string, std::pair<boost::shared_ptr<sink_custom_t>, std::set<thread_id_t>>>
        m_custom_map;

    static sev_lvl get_sev_lvl(const std::string& sevLvl);

    template <class... Args>
    BoostLog(const std::string& sevLvl, const Args&... args)
    {
        init(get_sev_lvl(sevLvl), args...);
    }

    template <class... Args>
    BoostLog(const Args&... args)
    {
        init(args...);
    }

    template <class... Args>
    BoostLog(const BoostLogConfig& config, const Args&... args)
    {
        init(get_sev_lvl(config.level), config.general_file, config.path, args...);
    }

    void init(const sev_lvl      sevLvl           = sev_lvl::warning,
              const std::string& general_filename = "general.log",
              const std::string& log_path         = "log/",
              const std::vector<std::pair<std::string, std::string>>& channels_files = {},
              const bool exclude_from_general = true, const std::string& custom_sink_type = "");

    BoostLog(BoostLog const&) = delete;
    BoostLog(BoostLog&&)      = delete;
    BoostLog& operator=(BoostLog const&) = delete;
    BoostLog& operator=(BoostLog&&) = delete;
};
} // namespace commtools

BOOST_LOG_INLINE_GLOBAL_LOGGER_INIT(logger, commtools::sev_ch_log)
{
    return commtools::sev_ch_log();
}

#ifdef LOG_CHANNEL
#define LOG(lvl) BOOST_LOG_CHANNEL_SEV(logger::get(), BL_TOSTRING(LOG_CHANNEL), lvl)
#endif

#ifndef LOG_CHANNEL
#define LOG(lvl) BOOST_LOG_CHANNEL_SEV(logger::get(), "Undef.Chan.", lvl)
#endif

#define LOG_CH(lvl, chan) BOOST_LOG_CHANNEL_SEV(logger::get(), chan, lvl)

#define BL_TRACE() LOG(commtools::sev_lvl::trace)
#define BL_FTRACE() BL_TRACE() << "call::" << __FUNCTION__ << " "
#define BL_DEBUG() LOG(commtools::sev_lvl::debug)
#define BL_INFO() LOG(commtools::sev_lvl::info)
#define BL_WARNING() LOG(commtools::sev_lvl::warning)
#define BL_ERROR() LOG(commtools::sev_lvl::error)
#define BL_CRITICAL() LOG(commtools::sev_lvl::critical)

#define BL_TRACE_CH(chan) LOG_CH(commtools::sev_lvl::trace, chan)
#define BL_DEBUG_CH(chan) LOG_CH(commtools::sev_lvl::debug, chan)
#define BL_INFO_CH(chan) LOG_CH(commtools::sev_lvl::info, chan)
#define BL_WARNING_CH(chan) LOG_CH(commtools::sev_lvl::warning, chan)
#define BL_ERROR_CH(chan) LOG_CH(commtools::sev_lvl::error, chan)
#define BL_CRITICAL_CH(chan) LOG_CH(commtools::sev_lvl::critical, chan)

#endif
